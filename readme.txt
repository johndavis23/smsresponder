Read Me

Live at http://heartsleeve.net

A simple SMS responder app; 

Uses codeigniter, MySQL, Bootstrap, TankAuth

Support for NoSQL MongoDB on SMS tables in progress.

Project: Retext

Scenario:  
RSC runs TV, radio, youtube, etc.. ad that promotes a particular product or special offer.  The ad’s call-to-action prompts the user to send a text message to a specified phone number and in return they get a link to the special offer.  For example:  Text “Dog Bone” to 865-444-2222 and get a response: http://www.store.petsafe.net/new_super_dog_bone_offer

Description:
Design, architect, and implement a solution that can fulfill this need.  The solution should allow us to dynamically set text message keywords and associated responses.  The solution should accept incoming text messages and provide the user with an appropriate response message.

Requirements:
- Application must be coded in PHP 5.3+
- Application must have an authentication system
- Application must use a MySQL database
- Application must allow user to manage keywords and associated responses
- Application must use Bootstrap
- Application must integrate with an SMS gateway provider such as Twilio
- Application must use GIT source control on a public GitHub repo.

Prefered:
- Application works with both an SQL and NoSQL database solution interchangeably.

Optional:
- Application can be built on some framework such as CodeIgniter




John Davis
johnericdavis@gmail.com