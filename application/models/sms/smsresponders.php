<?php 
/* 
 * 
 * Project:				PetSafe Code Interview
 * Version:				1.0
 * Date:				Febuary 15, 2014
 * Original Author:		John Davis 
 * 
 * Model: 				SmsResponders
 * Purpose: 			Model for interactions with keywords and responses via SMS
 * Fields:
 * 		keyword:		Text to respond to ; can be RegExp or Text.
 * 		response:		Response Text
 * 		enabled:		If false, ignore date_* and instead respond with default message. 
 * 						If true, process based off of dates; if outside range, default message.
 * 		language:		ID representing active language; For localization.
 * 
 * 
 */
class SmsResponders extends CI_Model 
{
	
    var $keyword   		= '';
    var $response 		= '';
	var $enabled 		= '';
	
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		 $this->load->database();
    }
    
	/*------------------------------------------------------------------
	 * 							GET 
	 * ------------------------------------------------------------------
	 */
  
	function get_all_entries()
    {
    	$this->config->load('sms');
		if($this->config->item('sms_mongo')) //check if we are using mongo TODO: It would be nice if this was more modular JED Build 2
		{
			try{
				$this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection')));
				
				
				 $results = $this->Builder->get('smsresponder');
				 if(!empty($results))
				 {	error_log($results);
					return $results;}
				return NULL;	
			}
			catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
		}
		else //sql or native engine
		{
	        $query = $this->db->get('smsresponders');
	        return $query->result();
		}
    }
	
	
	
	function get_smsentry_by_id($id) 
	{
		$this->config->load('sms');
		if($this->config->item('sms_mongo')) //mongo
		{
			try{
				$this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection')));
				
				
				 $results = $this->Builder->where('id', $id)
				    ->where('enabled', 1)
				    ->get('smsresponder');
				 if(!empty($results))
					return $results;
				return NULL;	
			}
			catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
		}
		else //sql or native engine
		{
			$this->db->where('id', $id);
			$query = $this->db->get('smsresponders'); 
			
			if ($query->num_rows() == 1) return $query->row();
			return NULL;
		}
	}
	
	
	
	function get_active_entry_by_keyword($keyword) 
	{
		$this->config->load('sms');
		if($this->config->item('sms_mongo')) //mongo
		{
			try{
				$this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection'))); //load mangodb class
				
				
				 $results = $this->Builder->where('keyword', $keyword)
				    ->where('enabled', 1)
				    ->get('smsresponder');
				 if(!empty($results))
					return $results;
				return NULL;	
			}
			catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
		}
		else //sql or native engine
		{
			$this->db->where('LOWER(keyword)=', strtolower($keyword));
			$this->db->where('enabled', 1);
			
			$query = $this->db->get('smsresponders');
			if ($query->num_rows() == 1) return $query->row();
			return NULL;
		}
				
	}
	
	/*------------------------------------------------------------------
	 * 							SET 
	 * ------------------------------------------------------------------
	 */
    function insert_entry()
    {
    	$this->config->load('sms');
		if($this->config->item('sms_mongo')) //mongo
		{
			try{
				//load mangodb class
				$this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection')));
				
				
				 $this->Builder->insert('smsresponder', array(
				    'keyword'  		=>  $this->input->post('keyword'),
				    'response'  	=>  $this->input->post('response'),
				    'enabled' 		=>  $this->input->post('enabled')
				));
			}catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
			
		}else //mysql or native engine
		{
			$this->keyword  		= $this->input->post('keyword');
	        $this->response 		= $this->input->post('response');
	       
			$this->enabled    		= $this->input->post('enabled');
			
	        $this->db->insert('smsresponders', $this);
		}
       
    }




    function update_entry()
    {
    	$this->config->load('sms');
    	if($this->config->item('sms_mongo')) //mongo
		{
			try
			{
				//load mangodb class
				 $this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection')));
				
				
				 $this->Builder->where(array('id' => $this->input->post('id')))->set(
				 	array(
					    'keyword'  		=>  $this->input->post('keyword'),
					    'response'  	=>  $this->input->post('response'),
					    'enabled' 		=>  $this->input->post('enabled')
				 	)
				 )->update('smsresponder');
			}catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
			
		}else //mysql or native engine
		{
			$this->keyword  		= $this->input->post('keyword');
	        $this->response 		= $this->input->post('response');
	        
			$this->enabled    		= $this->input->post('enabled');
	
	        $this->db->update('smsresponders', $this, array('id' => $this->input->post('id')));
		}
        
    }
	
	
	
	function delete_entry($id)
	{
		$this->config->load('sms');
		if($this->config->item('sms_mongo')) //mongo
		{
			try{
				//load mangodb class
				$this->load->library('Builder',array( 'dsn'   =>  $this->config->item('sms_mongo_connection')));
				$this->Builder->where(array('id' => $id))->delete('smsresponder');
			}catch (Exception $e) 
			{
			    show_error( 'Caught exception: '.$e->getMessage()."\n");
			}
			
		}else //mysql or native engine
		{
			$this->db->delete('smsresponders', array('id' =>  $id)); 
		}
		
	}
} 
?>