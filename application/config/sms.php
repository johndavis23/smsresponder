<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* Name:  SMS
	*
	* Author: John Davis
	*
	*
	* Description:  SMS configuration settings.
	*
	*
	*/

	/*
	 * SMS_MONGO
	 * use mangodb (NoSQL) for sms responders (authentication uses native engine)
	 * TODO: Still In Development
	 */
	$config['sms_mongo']   = false; 

	$config['sms_mongo_connection']   ='mongodb://login:password@localhost:27017/petsafesms';


/* End of file sms.php */