
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * 
 * Project:				PetSafe Code Interview
 * Version:				1.0
 * Date:				Febuary 15, 2014
 * Original Author:		John Davis 
 * 
 * Controller: 			Sms
 * Purpose: 			Controller for SMS entries
 * Functions:
 * 		index:			Provide a list
 * 		add:			Provide a form and process adding a record
 * 		update:			Provide a form and process updating a record
 * 		delete:			delete a record, redirect to list
 * 
 * 
 */
class Sms extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('security');
		$this->load->library('tank_auth');
		$this->lang->load('tank_auth');
	}

	function index()
	{
		if ($message = $this->session->flashdata('message')) {
			$this->load->view('auth/general_message', array('message' => $message));
		} else 
		{
			if ($this->tank_auth->is_logged_in())  // logged in
			{									
				
				$this->load->model('sms/smsresponders');
				
											
				$data['smsresponders'] = $this->smsresponders->get_all_entries();
			
				//grab localized specific elements
				$this->lang->load('sms', 'english');
				$data['title'] = $this->lang->line('sms_entries_list_title');
				$data['head'] = $this->lang->line('sms_entries_list_head');
				$data['lead'] = $this->lang->line('sms_entries_list_lead');
				
				$data['loggedin'] = true;
				
				$this->load->view('header', $data);
				$this->load->view('sms/list', $data);
				$this->load->view('footer', $data);
				
	
			} 
			elseif ($this->tank_auth->is_logged_in(FALSE)) 
			{						// logged in, not activated
				redirect('/auth/send_again/');
			} 
			else //not logged in
			{
				redirect('/auth/login/');
			}
			//redirect('/sms/add/');
		}
	}

	/**
	 * Add SMS Responder
	 *
	 * @return void
	 */
	function add()
	{
		if ($this->tank_auth->is_logged_in())  // logged in
		{									
			$this->form_validation->set_rules('keyword', 'Keyword', 'trim|required|xss_clean');
			$this->form_validation->set_rules('response', 'Response', 'trim|required|xss_clean');
			$this->form_validation->set_rules('enabled', 'Enabled', 'integer');

			
			$data['errors'] = array();

			
			if ($this->form_validation->run()) // validation ok
			{
				$this->load->model('sms/smsresponders');
											
				$this->smsresponders->insert_entry();
				redirect('/sms/');
				
			}else
			{
				$this->lang->load('sms', 'english');
				$data['title'] = $this->lang->line('sms_entries_add_title');
				$data['head'] = $this->lang->line('sms_entries_add_head');
				$data['lead'] = $this->lang->line('sms_entries_add_lead');
				
				$data['loggedin'] = true;
				
				$this->load->view('header', $data);
				$this->load->view('sms/insert_form', $data);
				$this->load->view('footer', $data);
			}

		} 
		elseif ($this->tank_auth->is_logged_in(FALSE)) 
		{						// logged in, not activated
			redirect('/auth/send_again/');
		} 
		else //not logged in
		{
			redirect('/auth/login/');
		}
	}

	/**
	 * Update SMS Responder
	 *
	 * @return void
	 */
	function update($id)
	{
		
		if ($this->tank_auth->is_logged_in())  // logged in
		{									
			$this->form_validation->set_rules('keyword', 'Keyword', 'trim|required|xss_clean');
			$this->form_validation->set_rules('response', 'Response', 'trim|required|xss_clean');
			$this->form_validation->set_rules('enabled', 'Enabled', 'integer');

			
			$data['errors'] = array();

			
			if ($this->form_validation->run()) // validation ok
			{
				$this->load->model('sms/smsresponders');
										
				$this->smsresponders->update_entry();
				redirect('/sms/');
				
			}else
			{
				$this->load->model('sms/smsresponders');
				$data['smsresponder'] = $this->smsresponders->get_smsentry_by_id($id);
				
				
				if($data['smsresponder'] != NULL) // make sure we got a record
				{
					$this->lang->load('sms', 'english');
					$data['title'] = $this->lang->line('sms_entries_update_title');
					$data['head'] = $this->lang->line('sms_entries_update_head');
					$data['lead'] = $this->lang->line('sms_entries_update_lead');
				
					$data['loggedin'] = true;
				
					$this->load->view('header', $data);
					$this->load->view('sms/update_form', $data);
					$this->load->view('footer', $data);
				}
				else //TODO 
				{
					$this->lang->load('sms', 'english');
					$data['title'] = $this->lang->line('sms_entries_update2_title');
					$data['head'] = $this->lang->line('sms_entries_update2_head');
					$data['lead'] = $this->lang->line('sms_entries_update2_lead');
					$data['loggedin'] = true;
					
					$this->load->view('header', $data);
					$this->load->view('sms/update_form', $data);
					$this->load->view('footer', $data);
				}
			}

		} 
		elseif ($this->tank_auth->is_logged_in(FALSE)) 
		{						// logged in, not activated
			redirect('/auth/send_again/');
		} 
		else //not logged in
		{
			redirect('/auth/login/');
		}
	}
	function delete($id)
	{
		if (!$this->tank_auth->is_logged_in()) {
			// not logged in or not activated
			redirect('/auth/login/');

		} else {
			$this->load->model('sms/smsresponders');
			$this->smsresponders->delete_entry($id);
			redirect('/sms/');
		}
	}
}