<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class twiliores extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	
	function index()
	{
		//load what we need
		$this->load->library('twilio');
		$this->load->helper('twilio-helper');
	
		$service = get_twilio_service();
		
		$body = $_REQUEST['Body'];
		
		/*  format $body */
		$result = preg_replace("/[^A-Za-z0-9]/u", " ", $body);
		$result = trim($result);
		$result = strtolower($result);
		
		//first grab the model and see if theres a match
		$this->load->model('sms/smsresponders');
		
		$entry = $this->smsresponders->get_active_entry_by_keyword($result);
		
		$text = "default";
		if($entry != NULL) //do we have a single match?
		{
			$text = $entry->response;
		}
		
		$response = new Services_Twilio_Twiml();
		$response->sms($text);
		echo $response;
	}
}
