<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!-- Optional theme -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
	<!-- Latest compiled and minified JavaScript 
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>-->
	  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
	<link rel="stylesheet" href="//d3taa8h1p8788q.cloudfront.net/media/css/screen.min.1312261.css" type="text/css" />
    <link rel="stylesheet" href="//d3taa8h1p8788q.cloudfront.net/media/bootstrap/css/bootstrap.min.css" type="text/css" />
    
	

	
   <link rel="stylesheet" href="/assets/css/style.css">
  
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">SMS Responder</a>
        </div>
        <div class="collapse navbar-collapse">
          
          <ul class="nav navbar-nav">
            <li><a href="/index.php/sms/">List</a></li>
            <li><a href="/index.php/sms/add">Add</a></li>
            <?php
            if(isset($loggedin)){
				echo '<li><a href="/index.php/auth/logout">Logout</a></li>';
			}
			else {
				echo '<li><a href="/index.php/auth/login">Login</a></li>';
			}
             ?>
            
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">

      <div class="starter-template" style="">
      	<a href="http://www.petsafe.net" id="logo">PetSafe</a>
		<h1><?php echo $head; ?></h1>
        <p class="lead"><?php echo $lead; ?></p>
      </div>