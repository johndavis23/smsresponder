<?php

/*
 * Initialize our form fields
 */
$keyword = array(
	'name'	=> 'keyword',
	'id'	=> 'keyword',
	'value' => set_value('keyword'),
	'maxlength'	=> 70, //assume non-latin alphabet for max size (160 for latin) in case of Chinese localization
	'size'	=> 30,
);

$response = array(
	'name'	=> 'response',
	'id'	=> 'response',
	'value' => set_value('response'),
	'maxlength'	=> 70, //assume non-latin alphabet for max size (160 for latin) in case of Chinese localization
	'size'	=> 30,
);

$enabled = array(
	'name'	=> 'enabled',
	'id'	=> 'enabled',
	'value'	=> 1,
	'checked'	=> set_value('enabled'),
	'style' => 'margin:0;padding:0',

);


$hidden = array( 'id' => set_value('id')); 
?>
<?php echo form_open($this->uri->uri_string(),'',$hidden); ?>
<table>
	<tr>
		<td><?php echo form_label('Keyword', $keyword['id']); ?></td>
		<td><?php echo form_input($keyword); ?></td>
		<td style="color: red;">
			<?php echo form_error($keyword['name']); ?>
			<?php echo isset($errors[$keyword['name']])?$errors[$keyword['name']]:''; ?>
		</td>
	</tr>
	<tr>
		<td><?php echo form_label('Response', $response['id']); ?></td>
		<td><?php echo form_input($response); ?></td>
		<td style="color: red;">
			<?php echo form_error($response['name']); ?>
			<?php echo isset($errors[$response['name']])?$errors[$response['name']]:''; ?>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<?php echo form_checkbox($enabled); ?>
			<?php echo form_label('Enabled', $enabled['id']); ?> 
			<?php ;/* echo anchor('/sms/delete/', 'Delete');*/ ?>
		</td><td><?php echo form_submit('submit', 'Add'); ?></td>
	</tr>
</table>

<?php echo form_close(); ?>