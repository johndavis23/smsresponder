<?php



$lang['sms_entries_list_title'] = "SMS PetSafe - List Entries";
$lang['sms_entries_list_head'] = "List of Existing Entries";
$lang['sms_entries_list_lead'] = "Below are listed all existing SMS Responders.";
$lang['sms_entries_add_title'] = "SMS PetSafe - Add Entry";
$lang['sms_entries_add_head'] = "Add Entry";
$lang['sms_entries_add_lead'] = "Add a new SMS Responder here.";
$lang['sms_entries_update_title'] = "SMS PetSafe - Update Entry";
$lang['sms_entries_update_head'] = "Update Entry";
$lang['sms_entries_update_lead'] = "Update an already exising SMS Responder here.";
$lang['sms_entries_update2_title'] = "SMS PetSafe - Update Entry";
$lang['sms_entries_update2_head'] = "Update Entry";
$lang['sms_entries_update2_lead'] = "Update an already exising SMS Responder here.";


/* End of file sms_lang.php */
/* Location: ./application/language/english/sms_lang.php */?>